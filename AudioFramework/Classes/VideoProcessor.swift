//
//  VideoProcessor.swift
//  AudioFramework
//
//  Created by Marius Avram on 6/24/19.
//

import UIKit
import AVKit

public class VideoProcessor: NSObject {
    @objc public static let sharedInstance = VideoProcessor()
    
    public func extractAudioAndExport(sourceUrl:URL, completionHandler:((Bool, String) -> Void)? = nil) {
        // Create a composition
        let composition = AVMutableComposition()
        do {
            let asset = AVURLAsset(url: sourceUrl)
            guard let audioAssetTrack = asset.tracks(withMediaType: AVMediaType.audio).first else { return }
            guard let audioCompositionTrack = composition.addMutableTrack(withMediaType: AVMediaType.audio, preferredTrackID: kCMPersistentTrackID_Invalid) else { return }
            try audioCompositionTrack.insertTimeRange(audioAssetTrack.timeRange, of: audioAssetTrack, at: CMTime.zero)
        } catch {
            print(error)
        }
        
        // Get url for output
        let outputUrl = URL(fileURLWithPath: NSTemporaryDirectory() + "out.m4a")
        if FileManager.default.fileExists(atPath: outputUrl.path) {
            try? FileManager.default.removeItem(atPath: outputUrl.path)
        }
        
        // Create an export session
        let exportSession = AVAssetExportSession(asset: composition, presetName: AVAssetExportPresetPassthrough)!
        exportSession.outputFileType = AVFileType.m4a
        exportSession.outputURL = outputUrl
        
        // Export file
        exportSession.exportAsynchronously {
            DispatchQueue.main.async {
                // Present a UIActivityViewController to share audio file
                if let handler = completionHandler {
                    if let error = exportSession.error {
                        handler(false,error.localizedDescription)
                    }
                    else if let url = exportSession.outputURL {
                        handler(true, url.path)
                    }
                    else {
                        handler(false,"Failed to export.")
                    }
                }
            }
        }
    }
    
    public func extractAudioAndExport(asset:AVAsset, completionHandler:((Bool, String) -> Void)? = nil) {
        // Create a composition
        let composition = AVMutableComposition()
        do {
            guard let audioAssetTrack = asset.tracks(withMediaType: AVMediaType.audio).first else { return }
            guard let audioCompositionTrack = composition.addMutableTrack(withMediaType: AVMediaType.audio, preferredTrackID: kCMPersistentTrackID_Invalid) else { return }
            try audioCompositionTrack.insertTimeRange(audioAssetTrack.timeRange, of: audioAssetTrack, at: CMTime.zero)
        } catch {
            print(error)
        }
        
        // Get url for output
        let outputUrl = URL(fileURLWithPath: NSTemporaryDirectory() + "out.m4a")
        if FileManager.default.fileExists(atPath: outputUrl.path) {
            try? FileManager.default.removeItem(atPath: outputUrl.path)
        }
        
        // Create an export session
        let exportSession = AVAssetExportSession(asset: composition, presetName: AVAssetExportPresetPassthrough)!
        exportSession.outputFileType = AVFileType.m4a
        exportSession.outputURL = outputUrl
        
        // Export file
        exportSession.exportAsynchronously {
            DispatchQueue.main.async {
                // Present a UIActivityViewController to share audio file
                if let handler = completionHandler {
                    if let error = exportSession.error {
                        handler(false,error.localizedDescription)
                    }
                    else if let url = exportSession.outputURL {
                        handler(true, url.path)
                    }
                    else {
                        handler(false,"Failed to export.")
                    }
                }
            }
        }
    }
}
