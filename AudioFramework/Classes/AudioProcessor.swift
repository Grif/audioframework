//
//  AudioProcessor.swift
//  AudioFramework
//
//  Created by Marius Avram on 6/24/19.
//

import UIKit
import AVKit
import SwiftVideoGenerator

public protocol AudioVideoProcessorProtocol:NSObjectProtocol {
    func progresUpdate(value:Float)
    func doneWithSuccess(url:URL)
    func failedWithError(error:String)
}

public class AudioProcessor: NSObject {
    
    @objc public static let sharedInstance = AudioProcessor()
    public weak var delegate:AudioVideoProcessorProtocol!

    public func audioToVideo(audioFileURL:URL, savePathUrl:URL, images:[UIImage], timeIntervals:[CMTime]) {
        VideoGenerator.fileName = savePathUrl.lastPathComponent
        VideoGenerator.shouldOptimiseImageForVideo = true
        VideoGenerator.videoImageWidthForMultipleVideoGeneration = 2000
        
        splitInAudiFiles(audioFileURL: audioFileURL, timeIntervals: timeIntervals) { (success, urls) in
            if success {
                VideoGenerator.current.generate(withImages: images, andAudios: urls, andType: .multiple, { (progress) in
                    print(progress)
                    DispatchQueue.main.async {
                        if self.delegate != nil {
                            self.delegate.progresUpdate(value: Float(progress.fractionCompleted))
                        }
                    }
                }) { (urlErrors) in//Result<URL, Error>) in
                    switch urlErrors {
                    case .success(let url):
                        if self.delegate != nil {
                            self.delegate.doneWithSuccess(url: url)
                        }
                        print(url)
                    case .failure(let error):
                        if self.delegate != nil {
                            self.delegate.failedWithError(error: error.localizedDescription)
                        }
                    }

//                        if let error = urlErrors. {
//                            print(error)
//                            if self.delegate != nil {
//                                self.delegate.failedWithError(error: error.localizedDescription)
//                            }
//                        }
//                        else {
//
//                        }
//                    }
                }
            }
        }
    }
    
    func splitInAudiFiles(audioFileURL:URL, timeIntervals:[CMTime], resultingURLs:[URL] = [URL](), completionHandler:((Bool, [URL]) -> Void)? = nil) {
        let avAsset = AVURLAsset(url: audioFileURL, options: nil)

        let fileName = "\(ProcessInfo.processInfo.globallyUniqueString).m4a"
        exportAsset(avAsset, fileName: fileName, start: timeIntervals[0], end: timeIntervals[1]) { (success) in
            if success {
                let documentsDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
                let trimmedSoundFileURL = documentsDirectory.appendingPathComponent(fileName)
                
                var localResultingURLs = resultingURLs
                localResultingURLs.append(trimmedSoundFileURL)
                
                var localIntervals = timeIntervals
                localIntervals.removeFirst()
                if localIntervals.count < 2 {
                    if completionHandler != nil {
                        completionHandler!(true,localResultingURLs)
                    }
                }
                else {
                    self.splitInAudiFiles(audioFileURL: audioFileURL, timeIntervals: localIntervals, resultingURLs: localResultingURLs, completionHandler: completionHandler)
                }
            }
            else {
                if completionHandler != nil {
                    completionHandler!(false,resultingURLs)
                }
            }
        }
    }
    
    func exportAsset(_ asset: AVAsset, fileName: String, start: CMTime, end: CMTime, completionHandler:((Bool) -> Void)?) {
        print("\(#function)")
        
        let documentsDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
        let trimmedSoundFileURL = documentsDirectory.appendingPathComponent(fileName)
        print("saving to \(trimmedSoundFileURL.absoluteString)")
        
        if FileManager.default.fileExists(atPath: trimmedSoundFileURL.absoluteString) {
            print("sound exists, removing \(trimmedSoundFileURL.absoluteString)")
            do {
                if try trimmedSoundFileURL.checkResourceIsReachable() {
                    print("is reachable")
                }
                
                try FileManager.default.removeItem(atPath: trimmedSoundFileURL.absoluteString)
            } catch {
                print("could not remove \(trimmedSoundFileURL)")
                print(error.localizedDescription)
            }
            
        }
        
        print("creating export session for \(asset)")
        
        if let exporter = AVAssetExportSession(asset: asset, presetName: AVAssetExportPresetAppleM4A) {
            exporter.outputFileType = AVFileType.m4a
            exporter.outputURL = trimmedSoundFileURL
            
            let duration = CMTimeGetSeconds(asset.duration)
            if duration < 5.0 {
                print("sound is not long enough")
                if completionHandler != nil {
                    completionHandler!(false)
                }
                return
            }
            // e.g. the first 5 seconds
            
            exporter.timeRange = CMTimeRangeFromTimeToTime(start: start, end: end)
            
            // do it
            exporter.exportAsynchronously(completionHandler: {
                print("export complete \(exporter.status)")
                
                switch exporter.status {
                case  AVAssetExportSessionStatus.failed:
                    
                    if let e = exporter.error {
                        print("export failed \(e)")
                    }
                    if completionHandler != nil {
                        completionHandler!(false)
                    }
                    
                case AVAssetExportSessionStatus.cancelled:
                    print("export cancelled \(String(describing: exporter.error))")
                    if completionHandler != nil {
                        completionHandler!(false)
                    }
                default:
                    print("export complete")
                    if completionHandler != nil {
                        completionHandler!(true)
                    }
                }
            })
        } else {
            print("cannot create AVAssetExportSession for asset \(asset)")
            if completionHandler != nil {
                completionHandler!(false)
            }
        }
        
    }
}
