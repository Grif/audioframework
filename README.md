# AudioFramework

[![CI Status](https://img.shields.io/travis/Marius Avram/AudioFramework.svg?style=flat)](https://travis-ci.org/Marius Avram/AudioFramework)
[![Version](https://img.shields.io/cocoapods/v/AudioFramework.svg?style=flat)](https://cocoapods.org/pods/AudioFramework)
[![License](https://img.shields.io/cocoapods/l/AudioFramework.svg?style=flat)](https://cocoapods.org/pods/AudioFramework)
[![Platform](https://img.shields.io/cocoapods/p/AudioFramework.svg?style=flat)](https://cocoapods.org/pods/AudioFramework)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

AudioFramework is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'AudioFramework'
```

## Author

Marius Avram, marius@codapper.com

## License

AudioFramework is available under the MIT license. See the LICENSE file for more info.
